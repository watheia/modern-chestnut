FROM node:14-alpine as builder

ARG NODE_ENV=production

# Uncomment if use of `process.dlopen` is necessary
# apk add --no-cache libc6-compat

ENV NODE_ENV $NODE_ENV

WORKDIR /usr/local/app

RUN mkdir public && echo '<p>Hello, World!</p>' > public/index.html

# COPY . .
# RUN yarn

FROM node:14-alpine

RUN npm install global --serve

COPY --from=builder /usr/local/app/public /usr/local/app 

ENV NODE_ENV $NODE_ENV

ENV PORT 5000
EXPOSE 50000

ENV PORT 5000
EXPOSE 50000

WORKDIR /usr/local/app

RUN [ "serve" ]

